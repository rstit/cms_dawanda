ActiveAdmin.register CmsSection do
  permit_params :name, :body

  index do
    selectable_column
    id_column
    column :name
    actions
  end

  filter :name

  form do |f|
    f.inputs "Section Details" do
      f.input :name
      f.input :body
    end
    f.actions
  end

end
