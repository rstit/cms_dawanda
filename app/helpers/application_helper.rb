require 'nokogiri'
require 'open-uri'

module ApplicationHelper
  def landing &block
    doc.css("article#main").first.inner_html = capture_haml(&block).html_safe
    doc.to_s.html_safe
  end

  def doc
    @document ||= Nokogiri::HTML(open('http://de.dawanda.com/'))
  end
end
