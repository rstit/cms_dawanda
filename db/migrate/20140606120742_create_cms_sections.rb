class CreateCmsSections < ActiveRecord::Migration
  def change
    create_table :cms_sections do |t|
      t.string :name
      t.text :body
      t.timestamps
    end
  end
end
